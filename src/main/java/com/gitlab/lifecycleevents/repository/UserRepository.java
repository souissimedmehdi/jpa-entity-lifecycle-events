package com.gitlab.lifecycleevents.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.lifecycleevents.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUserName(String userName);
}
